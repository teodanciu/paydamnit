package tzr
package paydamnit
package model

import org.joda.time.{DateTime, Duration, Period}
import Importance.Normal
import OccurrenceType._

case class Liability(
  id: Option[Long] = None,
  name: String,
  occurrenceType: OccurrenceType,
  warningDuration: Option[Duration],
  importance: Importance = Normal,
  url: Option[String] = None,
  account: Option[Account] = None,
  contact: Option[String] = None
)

case class Occurrence(
  id: Option[Long] = None,
  liabilityId: Long,
  metAt: Option[DateTime]
)

sealed trait OccurrenceType {
  val id: String
  def recurrence: Option[Period] = None
}

object OccurrenceType {
  case object Single extends OccurrenceType { val id = "single"}
  case class Recurrent(period: Period) extends OccurrenceType {
    val id = "recurrent"
    override def recurrence = Option(period)
  }
  case object Dynamic extends OccurrenceType { val id = "dynamic"}

  def fromString(tpe: String, period: Option[String]): OccurrenceType = (tpe, period) match {
    case ("single", _) => Single
    case ("recurrent", Some(p)) => Recurrent(Period.parse(p))
    case ("dynamic", _) => Dynamic
    case _ => throw new IllegalArgumentException(s"Cannot parse $tpe and $period to OccurrenceType")
  }
}

sealed trait Importance extends BaseEnum
object Importance extends EnumContainer[Importance]{
  case object Low extends Importance
  case object Normal extends Importance
  case object High extends Importance
  case object Critical extends Importance

  lazy val values: Set[Importance] = Set(Low, Normal, High, Critical)
}

case class Account(username: String, password: String)
