package tzr.paydamnit

package object model {

  trait BaseEnum {
    def id: String = toString.toLowerCase
  }

  trait EnumContainer[T <: BaseEnum] {
    def values: Set[T]

    def fromString(s: String): T = {
      val normalised = if (s == null) null else s.toLowerCase
      values.find(_.id == normalised).getOrElse(throw new IllegalArgumentException(s"Cannot parse $s to enum."))
    }
  }
}

