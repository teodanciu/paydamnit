package tzr
package paydamnit
package persist

import org.joda.time.{DateTime, Duration}

import com.github.tototoshi.slick.H2JodaSupport._
import scala.slick.driver.H2Driver.simple._

import tzr.paydamnit.model._

class Liabilities(tag: Tag) extends Table[Liability](tag, "liabilities") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def occurrenceType = column[String]("occurrenceType")
  def recurrence = column[Option[String]]("recurrence")
  def warningDuration = column[Option[String]]("warningDuration")
  def importance = column[String]("importance")
  def url = column[Option[String]]("url")
  def username = column[Option[String]]("username")
  def password = column[Option[String]]("password")
  def contact = column[Option[String]]("contact")

  def * = (id.?, name, occurrenceType, recurrence, warningDuration, importance, url, contact, username, password) <> (mapRow, unmapRow)

  private val mapRow: ((Option[Long], String, String, Option[String], Option[String], String, Option[String], Option[String], Option[String], Option[String])) => Liability = {
    case (id, name, occurrenceType, recurrence, warningDuration, importance, url, username, password, contact) =>
      Liability(
        id, name, OccurrenceType.fromString(occurrenceType, recurrence), warningDuration.map(Duration.parse),
        Importance.fromString(importance), url, username.flatMap(u => password.map(p => Account(u, p))), contact
      )
  }

  private val unmapRow = (l: Liability) => {
    Some(
      (l.id, l.name, l.occurrenceType.id, l.occurrenceType.recurrence.map(_.toString), l.warningDuration.map(_.toString), l.importance.id, l.url, l.account.map(_.username), l.account.map(_.password), l.contact)
    )
  }
}

class Occurrences(tag: Tag) extends Table[Occurrence](tag, "occurrences") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def liabilityId = column[Long]("liabilityId")
  def metAt = column[Option[DateTime]]("metAt")

  def * = (id.?, liabilityId, metAt) <> (Occurrence.tupled, Occurrence.unapply)
}