name := "paydamnit"

version := "1.0-SNAPSHOT"

resolvers += "Typesafe Simple Repository" at "http://repo.typesafe.com/typesafe/simple/maven-releases/"

libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "2.0.1",
  "org.joda" % "joda-money" % "0.9",
  "joda-time" % "joda-time" % "2.3",
  "org.joda" % "joda-convert" % "1.5",
  "com.github.tototoshi" %% "slick-joda-mapper" % "1.0.1",
  "org.scalatest"  %  "scalatest_2.10"%  "1.9.2"  %  "test"
)

